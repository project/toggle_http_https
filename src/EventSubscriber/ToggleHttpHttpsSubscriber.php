<?php

namespace Drupal\toggle_http_https\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ToggleHttpHttpsSubscriber implements EventSubscriberInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new ToggleHttpHttpsSubscriber.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Event handler for request processing. Redirects as needed.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   */
  public function checkRequestRedirection(GetResponseEvent $event) {

    $config = $this->configFactory->get('toggle_http_https.settings');
    if (php_sapi_name() != 'cli') {
      $url = $event->getRequest()->getUri();
      if ($event->getRequest()->isSecure() && $config->get('switch') == 'http' && $event->getRequest()->getScheme() == 'https') {
        $url = preg_replace("/^https:/i", "http:", $url);
        $event->setResponse(new RedirectResponse($url));
      }
      if (!$event->getRequest()->isSecure() && $config->get('switch') == 'https' && $event->getRequest()->getScheme() == 'http') {
        $url = preg_replace("/^http:/i", "https:", $url);
        $event->setResponse(new TrustedRedirectResponse($url));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('checkRequestRedirection');
    return $events;
  }

}
