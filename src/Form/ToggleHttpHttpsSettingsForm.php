<?php

namespace Drupal\toggle_http_https\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a settings for sharethis module.
 */
class ToggleHttpHttpsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['toggle_http_https.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'toggle_http_https_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['switch'] = array(
      '#type' => 'radios',
      '#options' => array(
        'http' => t('Switch to HTTP'),
        'https' => t('Switch to HTTPs'),
      ),
      '#default_value' => $this->config('toggle_http_https.settings')->get('switch'),
      '#title' => t('Toggle to HTTP/HTTPs'),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('toggle_http_https.settings');
    $config->set('switch', $values['switch'])
      ->save();
    parent::submitForm($form, $form_state);
  }

}
