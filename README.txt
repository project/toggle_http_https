CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------
The Toggle HTTP/HTTPs Set site to run on secure protocol or insecure protocol.

REQUIREMENTS
------------
This module requires none.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/documentation/install/modules-themes/modules-8
   for further information.

CONFIGURATION
-------------
 * Configure the Toggle HTTP/HTTPs Module settings in
   Administration » Configuration » System » Toggle HTTP/HTTPS configuration:
